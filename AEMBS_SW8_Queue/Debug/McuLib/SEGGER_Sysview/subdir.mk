################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../McuLib/SEGGER_Sysview/McuSystemView.c \
../McuLib/SEGGER_Sysview/SEGGER_SYSVIEW.c \
../McuLib/SEGGER_Sysview/SEGGER_SYSVIEW_Config_FreeRTOS.c \
../McuLib/SEGGER_Sysview/SEGGER_SYSVIEW_FreeRTOS.c 

OBJS += \
./McuLib/SEGGER_Sysview/McuSystemView.o \
./McuLib/SEGGER_Sysview/SEGGER_SYSVIEW.o \
./McuLib/SEGGER_Sysview/SEGGER_SYSVIEW_Config_FreeRTOS.o \
./McuLib/SEGGER_Sysview/SEGGER_SYSVIEW_FreeRTOS.o 

C_DEPS += \
./McuLib/SEGGER_Sysview/McuSystemView.d \
./McuLib/SEGGER_Sysview/SEGGER_SYSVIEW.d \
./McuLib/SEGGER_Sysview/SEGGER_SYSVIEW_Config_FreeRTOS.d \
./McuLib/SEGGER_Sysview/SEGGER_SYSVIEW_FreeRTOS.d 


# Each subdirectory must supply rules for building sources it contributes
McuLib/SEGGER_Sysview/%.o: ../McuLib/SEGGER_Sysview/%.c McuLib/SEGGER_Sysview/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DCPU_MK22FN512VLH12 -DCPU_MK22FN512VLH12_cm4 -DSDK_OS_BAREMETAL -DSERIAL_PORT_TYPE_UART=1 -DSDK_DEBUGCONSOLE=0 -DCR_INTEGER_PRINTF -DPRINTF_FLOAT_ENABLE=0 -D__MCUXPRESSO -D__USE_CMSIS -DDEBUG -I"D:\hslu\AEMBS\Workspace\AEMBS_SW8_Queue\drivers" -I"D:\hslu\AEMBS\Workspace\AEMBS_SW8_Queue\component\serial_manager" -I"D:\hslu\AEMBS\Workspace\AEMBS_SW8_Queue\utilities" -I"D:\hslu\AEMBS\Workspace\AEMBS_SW8_Queue\component\uart" -I"D:\hslu\AEMBS\Workspace\AEMBS_SW8_Queue\component\lists" -I"D:\hslu\AEMBS\Workspace\AEMBS_SW8_Queue\device" -I"D:\hslu\AEMBS\Workspace\AEMBS_SW8_Queue\CMSIS" -I"D:\hslu\AEMBS\Workspace\AEMBS_SW8_Queue\drivers" -I"D:\hslu\AEMBS\Workspace\AEMBS_SW8_Queue\component\serial_manager" -I"D:\hslu\AEMBS\Workspace\AEMBS_SW8_Queue\utilities" -I"D:\hslu\AEMBS\Workspace\AEMBS_SW8_Queue\component\uart" -I"D:\hslu\AEMBS\Workspace\AEMBS_SW8_Queue\component\lists" -I"D:\hslu\AEMBS\Workspace\AEMBS_SW8_Queue\device" -I"D:\hslu\AEMBS\Workspace\AEMBS_SW8_Queue\CMSIS" -I"D:\hslu\AEMBS\Workspace\AEMBS_SW8_Queue\board" -I"D:\hslu\AEMBS\Workspace\AEMBS_SW8_Queue\source" -I../McuLib -I../McuLib/config -I../McuLib/config/fonts -I../McuLib/fonts -I../McuLib/src -I../McuLib/FreeRTOS/Source/include -I../McuLib/FreeRTOS/Source/portable/GCC/ARM_CM4F -I../McuLib/SEGGER_RTT -I../McuLib/SEGGER_Sysview -I../McuLib/TraceRecorder -I../McuLib/TraceRecorder/config -I../McuLib/TraceRecorder/include -I../McuLib/TraceRecorder/streamports/Jlink_RTT/include -I../McuLib/HD44780 -I../McuLib/FatFS -I../McuLib/FatFS/source -include"D:/hslu/AEMBS/Workspace/AEMBS_SW8_Queue/source/IncludeMcuLibConfig.h" -O0 -fno-common -g3 -Wall -c -ffunction-sections -fdata-sections -ffreestanding -fno-builtin -fmerge-constants -fmacro-prefix-map="$(<D)/"= -mcpu=cortex-m4 -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -D__REDLIB__ -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


