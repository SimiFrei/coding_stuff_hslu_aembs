/*
 * Invader.c
 *
 *  Created on: 7 Apr 2022
 *      Author: simon
 */


#include "invader.h"
#include "McuRTOS.h"

static t_invader_event_e invader_event = Invader_Event_None;
void Invader_SendEvent(t_invader_event_e event){
	taskENTER_CRITICAL();
	if(invader_event != Invader_Event_None){
		vTaskDelay(pdMS_TO_TICKS(5));
	}
	invader_event = event;
	taskEXIT_CRITICAL();
}

t_invader_event_e Invader_ReceiveEvent(void){
	t_invader_event_e event;
	if(invader_event == Invader_Event_None){
		return Invader_Event_None;
	}
	taskENTER_CRITICAL();
	event = invader_event;
	invader_event = Invader_Event_None;
	taskEXIT_CRITICAL();
	return event;
}
