/*
 * I2C_Bus.h
 *
 *  Created on: 7 May 2022
 *      Author: simon
 */

#ifndef I2C_BUS_H_
#define I2C_BUS_H_

#include <stdbool.h>

void I2C_Bus_Init(void);
void I2C_Bus_DeInit(void);

bool I2C_Bus_Lock(void);
void I2C_Bus_UnLock(void);

#endif /* I2C_BUS_H_ */
