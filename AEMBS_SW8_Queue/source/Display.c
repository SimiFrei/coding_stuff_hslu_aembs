/*
 * Display.c
 *
 *  Created on: 7 May 2022
 *      Author: simon
 */

#include "display.h"
#include "McuSSD1306.h"
#include "McuGDisplaySSD1306.h"
#include "McuGFont.h"
#include "McuFontDisplay.h"
#include "McuUtility.h"
#include "McuFontHelv14Bold.h"
#include "McuFontHelv10Normal.h"
#include "McuFontHelv18Bold.h"
#include "I2C_Bus.h"
#include <stdio.h>

void Display_Init(void) {
	McuSSD1306_Init();
	McuGDisplaySSD1306_Init();
	McuGFont_Init();
	McuFontDisplay_Init();
}
void Display_DeInit(void) {
	McuSSD1306_Deinit();
	McuGDisplaySSD1306_Deinit();
	McuGFont_Deinit();
	McuFontDisplay_Deinit();
}

void Display_Update_View(void) {
	if (I2C_Bus_Lock() == false) {
		printf("i2c lock timed out");
	} else {
		McuGDisplaySSD1306_UpdateFull();
		I2C_Bus_UnLock();
	}
}
void Display_Clear(void) {
	McuGDisplaySSD1306_Clear();
	Display_Update_View();
}
void Display_DrawRectanlge(void) {
	Display_Clear();
	McuGDisplaySSD1306_DrawCircle(20, 30, 5, McuGDisplaySSD1306_COLOR_WHITE);
	Display_Update_View();
}
void Display_DrawCircle(void) {
	Display_Clear();
	McuGDisplaySSD1306_DrawBox(0, 0, McuGDisplaySSD1306_GetWidth() - 1,
	McuGDisplaySSD1306_GetHeight() - 1, 1,
	McuGDisplaySSD1306_COLOR_WHITE);
	Display_Update_View();
}
void Display_ShowSensorTemp(float value) {
	McuFontDisplay_PixelDim x, y, w, h;
	uint8_t buf[24];
	McuGDisplaySSD1306_Clear();
	McuUtility_NumFloatToStr(buf, sizeof(buf), value, 2);
	McuUtility_chcat(buf, sizeof(buf), 'C');
	w = McuFontDisplay_GetStringWidth(buf, McuFontHelv18Bold_GetFont(), NULL);
	h = McuFontDisplay_GetStringHeight(buf, McuFontHelv18Bold_GetFont(), NULL);
	x = (McuGDisplaySSD1306_GetWidth() - w) / 2;
	y = (McuGDisplaySSD1306_GetHeight() - h) / 2;
	McuFontDisplay_WriteString(buf, McuGDisplaySSD1306_COLOR_BLUE, &x, &y,
			McuFontHelv18Bold_GetFont());

	McuGDisplaySSD1306_DrawBox(0, 0, McuGDisplaySSD1306_GetWidth() - 1,
	McuGDisplaySSD1306_GetHeight() - 1, 1, McuGDisplaySSD1306_COLOR_WHITE);
	McuGDisplaySSD1306_DrawBox(2, 2, McuGDisplaySSD1306_GetWidth() - 5,
	McuGDisplaySSD1306_GetHeight() - 5, 1, McuGDisplaySSD1306_COLOR_WHITE);

	Display_Update_View();
}
void Display_ShowIntro(void);
