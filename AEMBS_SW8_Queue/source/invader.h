/*
 * invader.h
 *
 *  Created on: 7 Apr 2022
 *      Author: simon
 */

#ifndef INVADER_H_
#define INVADER_H_

typedef enum Invader_event_e {
	Invader_Event_None,
	Invader_Event_Button_Left_Pressed,
	Invader_Event_Button_Left_Repeat_Pressed,
	Invader_Event_Button_Left_Released,
	Invader_Event_Button_Right_Pressed,
	Invader_Event_Button_Right_Repeat_Pressed,
	Invader_Event_Button_Right_Released,
	Invader_Event_Button_Up_Pressed,
	Invader_Event_Button_Up_Repeat_Pressed,
	Invader_Event_Button_Up_Released,
	Invader_Event_Button_Down_Pressed,
	Invader_Event_Button_Down_Repeat_Pressed,
	Invader_Event_Button_Down_Released,
	Invader_Event_Button_Center_Pressed,
	Invader_Event_Button_Center_Repeat_Pressed,
	Invader_Event_Button_Center_Released
} t_invader_event_e;

void Invader_SendEvent(t_invader_event_e event);
t_invader_event_e Invader_ReceiveEvent(void);

#endif /* INVADER_H_ */
