/*
 * Copyright (c) 2022, Erich Styger
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "platform.h"
#include "blinky.h"
#include "McuLib.h"
#include "McuLED.h"
#include "McuWait.h"
#include "McuGPIO.h"
#include "McuButton.h"
#include "McuDebounce.h"
#include "McuRTOS.h"
#include "myTasks.h"
#include "buttons.h"
#include "debounce.h"
#include "McuRTT.h"
#include "McuSystemView.h"
#include "display.h"
#include "McuI2cLib.h"
#include "McuGenericI2C.h"
#include "MK22F51212.h"
#include "I2C_Bus.h"

void PL_Init(void) {
  McuLib_Init();
  McuRTOS_Init();
  McuWait_Init();
  McuGPIO_Init();
  McuLED_Init();
  //BLINKY_Init(); /* initialize blinky */
//  Cycles_Init(); /* initialize cycle counter */
//  LEDS_Init();
//  MyGpio_Init();

  McuBtn_Init();
  McuDbnc_Init();
  BTN_Init();
  Debounce_Init();

  McuGenericI2C_Init();
  CLOCK_EnableClock(kCLOCK_PortB);
  I2C_Bus_Init();
  McuI2cLib_Init();
  Display_Init();
  McuRTT_Init();
#if configUSE_SEGGER_SYSTEM_VIEWER_HOOKS
  McuSystemView_Init();
#endif
  MyTasksInit();
}

void PL_Deinit(void) {
  McuSystemView_Deinit();
  McuRTT_Deinit();
  Debounce_Deinit();
  BTN_Deinit();
  McuDbnc_Deinit();
  McuBtn_Deinit();
  MyTasks_Deinit();
  MyGpio_Deinit();
  LEDS_Deinit();
  Cycles_Deinit();
  //BLINKY_Deinit();
  McuLED_Init();
  McuGPIO_Deinit();
  McuWait_Deinit();
  McuRTOS_Deinit();
  McuLib_Deinit();
}
