/*
 * I2C_Bus.c
 *
 *  Created on: 7 May 2022
 *      Author: simon
 */


#include "I2C_Bus.h"
#include "McuRTOS.h"
#include <stdio.h>

static SemaphoreHandle_t mutex = NULL;

void I2C_Bus_Init(void){
	mutex = xSemaphoreCreateRecursiveMutex();
	vQueueAddToRegistry(mutex, "I2C_mutex");
}

void I2C_Bus_DeInit(void){
	vSemaphoreDelete(mutex);
}

bool I2C_Bus_Lock(void){
	if(xSemaphoreTakeRecursive(mutex, pdMS_TO_TICKS(20))!= pdTRUE){
		return false;
	}
	return true;
}
void I2C_Bus_UnLock(void){
	if(xSemaphoreGiveRecursive(mutex) != pdTRUE){
		for(;;);
	}
}
