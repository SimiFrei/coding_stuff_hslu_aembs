/*
 * display.h
 *
 *  Created on: 7 May 2022
 *      Author: simon
 */

#ifndef DISPLAY_H_
#define DISPLAY_H_


void Display_Init(void);
void Display_DeInit(void);

void Display_Update_View(void);
void Display_Clear(void);
void Display_DrawRectanlge(void);
void Display_DrawCircle(void);
void Display_ShowSensorTemp(float value);
void Display_ShowIntro(void);
#endif /* DISPLAY_H_ */
