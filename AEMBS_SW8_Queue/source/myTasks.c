/*
 * myTasks.c
 *
 *  Created on: 25 Mar 2022
 *      Author: simon
 */

#include "myTasks.h"
#include "pin_mux.h"
#include "McuRTOS.h"
#include "fsl_gpio.h"
#include <stdint.h>
#include <stdio.h>
#include "McuLib.h"
#include "McuWait.h"
#include "SystemView.h"
#include "invader.h"
#include "debounce.h"
#include "display.h"
static TaskHandle_t buttonTaskHandle = NULL;
static QueueHandle_t eventQueueHandle = NULL;
static void InvaderTask(void *pv) {

	t_invader_event_e event;
	float test = 12.0;
	for (;;) {
		test += 0.1;
		Display_ShowSensorTemp(test);
		vTaskDelay(pdMS_TO_TICKS(300));

	}
}

void MyTasksInit() {
	BaseType_t res;
	uint32_t param = 10;
	res = xTaskCreate(InvaderTask, "InvaderTask", 400 / sizeof(StackType_t),
			&param,
			configMAX_PRIORITIES - 1, &buttonTaskHandle);
	if (res != pdPASS) {
		/*error*/
		for (;;) {
		}
	}
	eventQueueHandle = Debounce_GetEventQueueHAndle();
	vTaskStartScheduler();
	// does not get here
}
