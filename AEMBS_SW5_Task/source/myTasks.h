/*
 * myTasks.h
 *
 *  Created on: 25 Mar 2022
 *      Author: simon
 */

#ifndef MYTASKS_H_
#define MYTASKS_H_

#include <stdint.h>
#include "board.h"

typedef struct blinkyParam {
	GPIO_Type *Gpio;
	uint32_t mask;
} blinkyParam_t;


void MyTasksInit(blinkyParam_t *param);

#endif /* MYTASKS_H_ */
