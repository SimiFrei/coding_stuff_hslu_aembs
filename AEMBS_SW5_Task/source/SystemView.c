/*
 * SystemView.c
 *
 *  Created on: 31 Mar 2022
 *      Author: simon
 */

#include "McuRTT.h"
#include "McuSystemView.h"
#include "SystemView.h"
void myInterrupt(void){
#if configUSE_SEGGER_SYSTEM_VIEWER_HOOKS
	McuSystemView_RecordEnterISR();
#endif
#if configUSE_SEGGER_SYSTEM_VIEWER_HOOKS
	McuSystemView_RecordExitISR();
#endif
}
void SystemView_Test(void){
	McuSystemView_Print((const char*)"Printing a message\r\n");
	McuSystemView_Warn((const char*)"Warning message\r\n");
	McuSystemView_Error((const char*)"Error message\r\n");

#if configUSE_SEGGER_SYSTEM_VIEWER_HOOKS
	McuSystemView_OnUserStart(1);
#endif
	McuWait_Waitms(10);
#if configUSE_SEGGER_SYSTEM_VIEWER_HOOKS
	McuSystemView_OnUserStop(1);
#endif
}
void SystemView_Init(void){
	McuRTT_Init();
#if configUSE_SEGGER_SYSTEM_VIEWER_HOOKS
	McuSystemView_Init();
#endif
}

