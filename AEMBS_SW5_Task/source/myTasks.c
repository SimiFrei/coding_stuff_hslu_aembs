/*
 * myTasks.c
 *
 *  Created on: 25 Mar 2022
 *      Author: simon
 */

#include "myTasks.h"
#include "pin_mux.h"
#include "McuRTOS.h"
#include "fsl_gpio.h"
#include <stdint.h>
#include <stdio.h>
#include "SystemView.h"

static TaskHandle_t blinkyTaskHandle = NULL;

static void blinkyTask(void *pv) {
	TickType_t xLastWakeTime = xTaskGetTickCount();
	blinkyParam_t *param = (blinkyParam_t*) pv;
	SystemView_Test();
	for (;;) {
		GPIO_PortToggle(param->Gpio, param->mask);
		vTaskDelayUntil(&xLastWakeTime, pdMS_TO_TICKS(100));
	}
}

void MyTasksInit(blinkyParam_t *param) {
	BaseType_t res;
	res = xTaskCreate(blinkyTask, "blinkyTask1", 400 / sizeof(StackType_t),
			param, configMAX_PRIORITIES - 1, &blinkyTaskHandle);
	if (res != pdPASS) {
		/*error*/
		for (;;) {
		}
	}

	vTaskStartScheduler();
	// does not get here
}
