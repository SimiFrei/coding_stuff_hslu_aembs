/*
 * SystemView.h
 *
 *  Created on: 31 Mar 2022
 *      Author: simon
 */

#ifndef SYSTEMVIEW_H_
#define SYSTEMVIEW_H_

void myInterrupt(void);
void SystemView_Test(void);
void SystemView_Init(void);
#endif /* SYSTEMVIEW_H_ */
