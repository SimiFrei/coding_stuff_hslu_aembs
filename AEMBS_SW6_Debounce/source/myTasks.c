/*
 * myTasks.c
 *
 *  Created on: 25 Mar 2022
 *      Author: simon
 */

#include "myTasks.h"
#include "pin_mux.h"
#include "McuRTOS.h"
#include "fsl_gpio.h"
#include <stdint.h>
#include <stdio.h>
#include "McuLib.h"
#include "McuWait.h"
#include "SystemView.h"

static TaskHandle_t buttonTaskHandle = NULL;

static void buttonTask(void *pv) {
	TickType_t xLastWakeTime = xTaskGetTickCount();
//	SystemView_Test();

	for (;;) {
		GPIO_PortToggle(BOARD_INITPINS_ledBlue_GPIO, BOARD_INITPINS_ledBlue_GPIO_PIN_MASK);
		vTaskDelayUntil(&xLastWakeTime, pdMS_TO_TICKS(100));
	}
}

void MyTasksInit() {
	BaseType_t res;
	uint32_t param = 10;
	res = xTaskCreate(buttonTask, "buttonTask", 400 / sizeof(StackType_t),
			&param, configMAX_PRIORITIES - 1, &buttonTaskHandle);
	if (res != pdPASS) {
		/*error*/
		for (;;) {
		}
	}

	vTaskStartScheduler();
	// does not get here
}
