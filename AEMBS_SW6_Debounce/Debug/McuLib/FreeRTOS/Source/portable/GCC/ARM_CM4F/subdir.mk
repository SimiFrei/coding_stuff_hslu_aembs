################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
S_SRCS += \
../McuLib/FreeRTOS/Source/portable/GCC/ARM_CM4F/portasm.s 

C_SRCS += \
../McuLib/FreeRTOS/Source/portable/GCC/ARM_CM4F/port.c 

OBJS += \
./McuLib/FreeRTOS/Source/portable/GCC/ARM_CM4F/port.o \
./McuLib/FreeRTOS/Source/portable/GCC/ARM_CM4F/portasm.o 

C_DEPS += \
./McuLib/FreeRTOS/Source/portable/GCC/ARM_CM4F/port.d 


# Each subdirectory must supply rules for building sources it contributes
McuLib/FreeRTOS/Source/portable/GCC/ARM_CM4F/%.o: ../McuLib/FreeRTOS/Source/portable/GCC/ARM_CM4F/%.c McuLib/FreeRTOS/Source/portable/GCC/ARM_CM4F/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DCPU_MK22FN512VLH12 -DCPU_MK22FN512VLH12_cm4 -DSDK_OS_BAREMETAL -DSERIAL_PORT_TYPE_UART=1 -DSDK_DEBUGCONSOLE=0 -DCR_INTEGER_PRINTF -DPRINTF_FLOAT_ENABLE=0 -D__MCUXPRESSO -D__USE_CMSIS -DDEBUG -I"D:\hslu\AEMBS\Workspace\AEMBS_SW6_Debounce\drivers" -I"D:\hslu\AEMBS\Workspace\AEMBS_SW6_Debounce\component\serial_manager" -I"D:\hslu\AEMBS\Workspace\AEMBS_SW6_Debounce\utilities" -I"D:\hslu\AEMBS\Workspace\AEMBS_SW6_Debounce\component\uart" -I"D:\hslu\AEMBS\Workspace\AEMBS_SW6_Debounce\component\lists" -I"D:\hslu\AEMBS\Workspace\AEMBS_SW6_Debounce\device" -I"D:\hslu\AEMBS\Workspace\AEMBS_SW6_Debounce\CMSIS" -I"D:\hslu\AEMBS\Workspace\AEMBS_SW6_Debounce\drivers" -I"D:\hslu\AEMBS\Workspace\AEMBS_SW6_Debounce\component\serial_manager" -I"D:\hslu\AEMBS\Workspace\AEMBS_SW6_Debounce\utilities" -I"D:\hslu\AEMBS\Workspace\AEMBS_SW6_Debounce\component\uart" -I"D:\hslu\AEMBS\Workspace\AEMBS_SW6_Debounce\component\lists" -I"D:\hslu\AEMBS\Workspace\AEMBS_SW6_Debounce\device" -I"D:\hslu\AEMBS\Workspace\AEMBS_SW6_Debounce\CMSIS" -I"D:\hslu\AEMBS\Workspace\AEMBS_SW6_Debounce\board" -I"D:\hslu\AEMBS\Workspace\AEMBS_SW6_Debounce\source" -I../McuLib -I../McuLib/config -I../McuLib/config/fonts -I../McuLib/fonts -I../McuLib/src -I../McuLib/FreeRTOS/Source/include -I../McuLib/FreeRTOS/Source/portable/GCC/ARM_CM4F -I../McuLib/SEGGER_RTT -I../McuLib/SEGGER_Sysview -I../McuLib/TraceRecorder -I../McuLib/TraceRecorder/config -I../McuLib/TraceRecorder/include -I../McuLib/TraceRecorder/streamports/Jlink_RTT/include -I../McuLib/HD44780 -I../McuLib/FatFS -I../McuLib/FatFS/source -include"D:/hslu/AEMBS/Workspace/AEMBS_SW6_Debounce/source/IncludeMcuLibConfig.h" -O0 -fno-common -g3 -Wall -c -ffunction-sections -fdata-sections -ffreestanding -fno-builtin -fmerge-constants -fmacro-prefix-map="$(<D)/"= -mcpu=cortex-m4 -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -D__REDLIB__ -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

McuLib/FreeRTOS/Source/portable/GCC/ARM_CM4F/%.o: ../McuLib/FreeRTOS/Source/portable/GCC/ARM_CM4F/%.s McuLib/FreeRTOS/Source/portable/GCC/ARM_CM4F/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: MCU Assembler'
	arm-none-eabi-gcc -c -x assembler-with-cpp -D__REDLIB__ -I"D:\hslu\AEMBS\Workspace\AEMBS_SW6_Debounce\board" -I"D:\hslu\AEMBS\Workspace\AEMBS_SW6_Debounce\source" -g3 -mcpu=cortex-m4 -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -D__REDLIB__ -specs=redlib.specs -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


