/*
 * blinky.c
 *
 *  Created on: 25 Mar 2022
 *      Author: simon
 */

#include "blinky.h"
#include "McuWait.h"
#include "McuLib.h"
#include "board.h"
#include "pin_mux.h"
#include "fsl_common.h"
#include "fsl_gpio.h"
#include "McuLED.h"

void Blinky_Run(void){
//	GPIO_PortClear(BOARD_INITPINS_ledBlue_GPIO, BOARD_INITPINS_ledBlue_GPIO_PIN_MASK);
//	McuWait_Waitms(1000);
//	GPIO_PortSet(BOARD_INITPINS_ledBlue_GPIO, BOARD_INITPINS_ledBlue_GPIO_PIN_MASK);
//	McuWait_Waitms(1000);
	GPIO_PortToggle(BOARD_INITPINS_ledBlue_GPIO, BOARD_INITPINS_ledBlue_GPIO_PIN_MASK);
	McuWait_Waitms(1000);
}

void Blinky_DeInit(void){
	McuWait_Deinit();
	McuLib_Deinit();
}
void BlinkyInit(void){
	McuLib_Init();
	McuWait_Init();
}
