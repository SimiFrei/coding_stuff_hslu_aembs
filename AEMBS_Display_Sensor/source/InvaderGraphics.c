/*
 * InvaderGraphics.c
 *
 *  Created on: 02.05.2022
 *      Author: simon
 */
#include "InvaderGraphics.h"
#include "display.h"
#include "McuSSD1306.h"
#include "McuGFont.h"
#include "McuFontDisplay.h"
#include "McuUtility.h"
#include "McuFontHelv14Bold.h"
#include "McuFontHelv08Normal.h"
#include "McuFontHelv10Normal.h"
#include "McuFontHelv10Bold.h"
#include "images/ship.h"
#include "images/missile.h"
#include "images/alien0.h"
#include "images/boom0.h"
#include "images/boom1.h"
#include "sprite.h"
#include "McuRTOS.h"
#include <stdio.h>
#include <stdbool.h>

static SpriteImage_t *shipImage;
static Sprite_t *ship, *missiles, *aliens, *booms, *alienMissiles;
static SemaphoreHandle_t missilesMutex = NULL;
static SemaphoreHandle_t aliensMutex = NULL;
static SemaphoreHandle_t boomMutex = NULL;
static SemaphoreHandle_t alienMissilesMutex = NULL;
static uint8_t aliensAlive = 0;
static bool shipHit = false;
/* module initialization */
void InvaderGraphics_Init(void) {
	Display_Init();
	missilesMutex = xSemaphoreCreateRecursiveMutex();
	aliensMutex = xSemaphoreCreateRecursiveMutex();
	boomMutex = xSemaphoreCreateRecursiveMutex();
	alienMissilesMutex = xSemaphoreCreateRecursiveMutex();
	vQueueAddToRegistry(missilesMutex, "Invader missiles mutex");
	vQueueAddToRegistry(aliensMutex, "aliens Mutex");
	vQueueAddToRegistry(boomMutex, "boom mutex");
	vQueueAddToRegistry(alienMissilesMutex, "alien missiles mutex");

}

static bool AliensMutex_Lock(void) {
	if (xSemaphoreTakeRecursive(aliensMutex, pdMS_TO_TICKS(20)) != pdTRUE) {
		return false;
	}
	return true;
}

void AliensMutex_Unlock(void) {
	/* give back mutex */
	if (xSemaphoreGiveRecursive(aliensMutex) != pdTRUE) {
		/* issue */
		printf("Could not give back aliens mutex..");
		for (;;)
			;
	}
}
void InvaderGraphics_AlienInit(void) {
	uint8_t nbrAliensRow = 0;

	nbrAliensRow = McuGDisplaySSD1306_GetWidth()
			/ (Alien0_GetImage()->width + 1);
	aliensAlive = nbrAliensRow;
	for (uint8_t i = 0; i < nbrAliensRow; i++) {
		SpriteImage_t *alienImage = Sprite_NewImage(Alien0_GetImage());
		if (alienImage != NULL) {
			Sprite_t *alien = Sprite_New(alienImage,
					(alienImage->image->width + 1) * i,
					alienImage->image->height);
			if (alien != NULL && AliensMutex_Lock()) {
				Sprite_AddToList(&aliens, alien);
				AliensMutex_Unlock();
			}
		}
	}
}
uint8_t GetAliensAlive() {
	return aliensAlive;
}
bool GetShipHit() {
	return shipHit;
}
void GetAlienPosition(uint8_t *pos, uint8_t index) {
	pos[2] = 255;
	if (AliensMutex_Lock()) {
		Sprite_t *alien = aliens;
		for (uint8_t i = 1; i < index; i++) {
			alien = alien->next;
		}
		if (alien != NULL) {
			pos[0] = alien->posX;
			pos[1] = alien->posY;
			pos[2] = 0;
		}
		AliensMutex_Unlock();
	}

}
/* module de-initialization */
void InvaderGraphics_Deinit(void) {
	Display_Deinit();

	vSemaphoreDelete(missilesMutex);
}

static bool MissilesMutex_Lock(void) {
	/* aquire mutex */
	if (xSemaphoreTakeRecursive(missilesMutex, pdMS_TO_TICKS(20)) != pdTRUE) {
		return false; /* timeout? */
	}
	return true;
}

static bool BoomMutex_Lock(void) {
	/* aquire mutex */
	if (xSemaphoreTakeRecursive(boomMutex, pdMS_TO_TICKS(20)) != pdTRUE) {
		return false; /* timeout? */
	}
	return true;
}

static bool AliensMissileMutex_Lock(void) {
	/* aquire mutex */
	if (xSemaphoreTakeRecursive(alienMissilesMutex, pdMS_TO_TICKS(20)) != pdTRUE) {
		return false; /* timeout? */
	}
	return true;
}

void MissilesMutex_Unlock(void) {
	/* give back mutex */
	if (xSemaphoreGiveRecursive(missilesMutex) != pdTRUE) {
		/* issue */
		printf("Could not give back missiles mutex..");
		for (;;)
			;
	}
}

void BoomMutex_Unlock(void) {
	/* give back mutex */
	if (xSemaphoreGiveRecursive(boomMutex) != pdTRUE) {
		/* issue */
		printf("Could not give back boom mutex..");
		for (;;)
			;
	}
}

void AliensMissileMutex_Unlock(void) {
	/* give back mutex */
	if (xSemaphoreGiveRecursive(alienMissilesMutex) != pdTRUE) {
		/* issue */
		printf("Could not give back aliens missiles mutex..");
		for (;;)
			;
	}
}

static void CheckAndRemoveObjects(void) {
	Sprite_t *m, *nextM, *a, *nextA, *b, *nextB, *am, *nextAm;
	uint8_t alienHit = 0;
	if (MissilesMutex_Lock() && AliensMutex_Lock()
			&& AliensMissileMutex_Lock()) {
		/* mutex aquired successfully */
		m = missiles;
		while (m != NULL) {
			/* if hit alien */
			a = aliens;
			while (a != NULL && alienHit == 0) {
				if (Sprite_Overlap(a, m)) {
					alienHit = 1;
					nextM = m->next;
					nextA = a->next;
					InvaderGraphics_DrawExplosion(a->posX, a->posY);
					Sprite_DeleteFromList(&aliens, a);
					Sprite_DeleteFromList(&missiles, m);
					m = nextM;
					a = nextA;
					aliensAlive--;
				} else {
					a = a->next;
				}

			}
			if (alienHit == 0) {
				/* if hit the border */
				if (m->posY <= 0) {
					nextM = m->next;  // remember following next element
					Sprite_DeleteFromList(&missiles, m); // remove element
					m = nextM;   // set iterator to current element
				} else {
					m = m->next;  // go to next element
				}
			} else {
				alienHit = 0;
			}
		}
		if (BoomMutex_Lock()) {
			b = booms;
			while (b != NULL) {
				if (b->anim.cntImagesChanges >= 2) {
					nextB = b->next;
					Sprite_DeleteFromList(&booms, b);
					b = nextB;
				} else {
					b = b->next;
				}
			}
			BoomMutex_Unlock();
		}
		am = alienMissiles;
		while (am != NULL) {
			if (Sprite_Overlap(am, ship)) {
				nextAm = am->next;
				InvaderGraphics_DrawExplosion(ship->posX, ship->posY);
				Sprite_DeleteFromList(&alienMissiles, am);
				Sprite_DeleteList(&missiles);
				ship->images = NULL;
				shipHit = true;
				am = nextAm;
				break;
			} else if (am->posY
					>= (McuGDisplaySSD1306_GetHeight()
							- ship->images->image->height / 2)) {
				/* hit the border */
				nextAm = am->next;
				Sprite_DeleteFromList(&alienMissiles, am);
				am = nextAm;
			} else {
				am = am->next;
			}
		}
		MissilesMutex_Unlock();
		AliensMutex_Unlock();
		AliensMissileMutex_Unlock();
	}

}

void InvaderGraphics_UpdateView(void) {
	McuGDisplaySSD1306_Clear(); /* blank screen */
	Sprite_Paint(ship); /* update all objects */
	if (AliensMutex_Lock()) {
		Sprite_PaintList(aliens);
		AliensMutex_Unlock();
	}
	if (MissilesMutex_Lock()) {
		Sprite_PaintList(missiles);
		MissilesMutex_Unlock();
	}
	if (BoomMutex_Lock()) {
		Sprite_PaintList(booms);
		BoomMutex_Unlock();
	}
	if (AliensMissileMutex_Lock()) {
		Sprite_PaintList(alienMissiles);
		AliensMissileMutex_Unlock();
	}
	CheckAndRemoveObjects();
	Display_UpdateView(); /* transmit data to display */
}

void InvaderGraphics_MoveShip(Ship_MoveDirection_e direction) {
	switch (direction) {
	case Ship_MoveDir_Left:
		Sprite_MoveLimited(ship, -5, 0, 0, McuGDisplaySSD1306_GetWidth(), 0,
		McuGDisplaySSD1306_GetHeight());
		break;
	case Ship_MoveDir_Right:
		Sprite_MoveLimited(ship, 5, 0, 0, McuGDisplaySSD1306_GetWidth(), 0,
		McuGDisplaySSD1306_GetHeight());
		break;
	case Ship_MoveDir_Down:
		Sprite_MoveLimited(ship, 0, 5, 0, McuGDisplaySSD1306_GetWidth(), 0,
		McuGDisplaySSD1306_GetHeight());
		break;
	case Ship_MoveDir_Up:
		Sprite_MoveLimited(ship, 0, -5, 0, McuGDisplaySSD1306_GetWidth(), 0,
		McuGDisplaySSD1306_GetHeight());
		break;
	case Ship_MoveDir_Unknown:
	default:
		break;
	}
}

void InvaderGraphics_ShipShootMissile() {
//	Sprite_t *alienB = aliens;
	SpriteImage_t *missileImage = Sprite_NewImage(Missile_GetImage());
	if (missileImage != NULL) {
		Sprite_t *missile = Sprite_New(missileImage,
				ship->posX + ship->images->image->width / 2
						- missileImage->image->width / 2,
				ship->posY - missileImage->image->height);
		if (missile != NULL) {
			missile->move.x = 0;
			missile->move.y = -1; //moving from bottom to top
			if (MissilesMutex_Lock()) {
				Sprite_AddToList(&missiles, missile);
				MissilesMutex_Unlock();
			}
		}
	}
}
void InvaderGraphics_AlienShootMissile(McuGDisplaySSD1306_PixelDim x,
		McuGDisplaySSD1306_PixelDim y) {
	SpriteImage_t *missileImage = Sprite_NewImage(Missile_GetImage());
	if (missileImage != NULL) {
		Sprite_t *missile = Sprite_New(missileImage,
				x + Alien0_GetImage()->width / 2 - missileImage->image->width,
				y + missileImage->image->height + Alien0_GetImage()->height);
		if (missile != NULL) {
			missile->move.x = 0;
			missile->move.y = 1;
			if (AliensMissileMutex_Lock()) {
				Sprite_AddToList(&alienMissiles, missile);
				AliensMissileMutex_Unlock();
			}
		}
	}
}
void InvaderGraphics_DrawExplosion(McuGDisplaySSD1306_PixelDim x,
		McuGDisplaySSD1306_PixelDim y) {
	SpriteImage_t *boomImage0 = Sprite_NewImage(Boom0_GetImage());
	SpriteImage_t *boomImage1 = Sprite_NewImage(Boom1_GetImage());
	if (boomImage0 != NULL && boomImage1 != NULL) {
		Sprite_t *boom = Sprite_New(boomImage0, x, y);
		if (boom != NULL) {
			Sprite_ImageAddToList(&boom->images, boomImage1);
			boom->anim.animate = true;
			boom->anim.delayCntr = 2;
			boom->anim.delayCntrReload = 3;
			boom->anim.cntImagesChanges = 0;
			if (BoomMutex_Lock()) {
				Sprite_AddToList(&booms, boom);
				BoomMutex_Unlock();
			}
		}
	}
}

void InvaderGraphics_ShowIntro(void) {
	McuFontDisplay_PixelDim x, y, w_title, w_sub, h_title, h_sub;
	uint8_t title[] = "Invader";
	uint8_t subtitle[] = "Press button!";

	McuGDisplaySSD1306_Clear();
	w_title = McuFontDisplay_GetStringWidth(title, McuFontHelv14Bold_GetFont(),
	NULL);
	h_title = McuFontDisplay_GetStringHeight(title, McuFontHelv14Bold_GetFont(),
	NULL);
	w_sub = McuFontDisplay_GetStringWidth(subtitle,
			McuFontHelv10Normal_GetFont(), NULL);
	h_sub = McuFontDisplay_GetStringHeight(subtitle,
			McuFontHelv10Normal_GetFont(), NULL);
	// calc position for title and write
	x = (McuGDisplaySSD1306_GetWidth() - w_title) / 2;
	y = (h_title / 2);
	McuFontDisplay_WriteString(title, McuGDisplaySSD1306_COLOR_BLUE, &x, &y,
			McuFontHelv14Bold_GetFont());

	// calc position for subtitle and write
	x = (McuGDisplaySSD1306_GetWidth() - w_sub) / 2;
	y += (h_title + h_sub / 2);
	McuFontDisplay_WriteString(subtitle, McuGDisplaySSD1306_COLOR_BLUE, &x, &y,
			McuFontHelv10Normal_GetFont());

	McuGDisplaySSD1306_DrawBox(0, 0, McuGDisplaySSD1306_GetWidth() - 1,
	McuGDisplaySSD1306_GetHeight() - 1, 1, McuGDisplaySSD1306_COLOR_WHITE);
	McuGDisplaySSD1306_DrawBox(2, 2, McuGDisplaySSD1306_GetWidth() - 5,
	McuGDisplaySSD1306_GetHeight() - 5, 1, McuGDisplaySSD1306_COLOR_WHITE);

	Display_UpdateView();
}

void InvaderGraphics_ShowEnd(bool lost) {
	McuFontDisplay_PixelDim x, y, w_title, w_sub, h_title, h_sub;
	uint8_t title[20] = { 0 };
	uint8_t subtitle[25] = { 0 };
	if (lost == false) {
		McuUtility_strcat(title, sizeof(title),
				(const unsigned char*) "Congratulations");
		McuUtility_strcat(subtitle, sizeof(subtitle),
				(const unsigned char*) "All Aliens killed");
	} else {
		McuUtility_strcat(title, sizeof(title),
				(const unsigned char*) "FAILED");
		McuUtility_strcat(subtitle, sizeof(subtitle),
				(const unsigned char*) " Aliens survived");
	}

	McuGDisplaySSD1306_Clear();
	w_title = McuFontDisplay_GetStringWidth(title, McuFontHelv10Bold_GetFont(),
	NULL);
	h_title = McuFontDisplay_GetStringHeight(title, McuFontHelv10Bold_GetFont(),
	NULL);
	w_sub = McuFontDisplay_GetStringWidth(subtitle,
			McuFontHelv08Normal_GetFont(), NULL);
	h_sub = McuFontDisplay_GetStringHeight(subtitle,
			McuFontHelv08Normal_GetFont(), NULL);
	// calc position for title and write
	x = (McuGDisplaySSD1306_GetWidth() - w_title) / 2;
	y = (h_title / 2);
	McuFontDisplay_WriteString(title, McuGDisplaySSD1306_COLOR_BLUE, &x, &y,
			McuFontHelv10Bold_GetFont());

	// calc position for subtitle and write

	x = (McuGDisplaySSD1306_GetWidth() - w_sub) / 2;
	y += (h_title + h_sub / 2);
	McuFontDisplay_WriteString(subtitle, McuGDisplaySSD1306_COLOR_BLUE, &x, &y,
			McuFontHelv08Normal_GetFont());
	if (lost) {
		uint8_t buf[5];
		McuUtility_Num8uToStr(buf, sizeof(buf), aliensAlive);
		x = (McuGDisplaySSD1306_GetWidth()
				- McuFontDisplay_GetStringWidth(buf,
						McuFontHelv08Normal_GetFont(), NULL)) / 2;
		y += h_sub + h_sub / 2;
		McuFontDisplay_WriteString(buf, McuGDisplaySSD1306_COLOR_BLUE, &x, &y,
				McuFontHelv08Normal_GetFont());
	}
	McuGDisplaySSD1306_DrawBox(0, 0, McuGDisplaySSD1306_GetWidth() - 1,
	McuGDisplaySSD1306_GetHeight() - 1, 1, McuGDisplaySSD1306_COLOR_WHITE);
	McuGDisplaySSD1306_DrawBox(2, 2, McuGDisplaySSD1306_GetWidth() - 5,
	McuGDisplaySSD1306_GetHeight() - 5, 1, McuGDisplaySSD1306_COLOR_WHITE);

	Display_UpdateView();
}
bool InvaderGraphics_CheckNoAlienLeft(void) {
	bool noAliensLeft = false;
	if (AliensMutex_Lock()) {
		if (aliens == NULL) {
			noAliensLeft = true;
		}
		AliensMutex_Unlock();
	}
	return noAliensLeft;

}

bool InvaderGraphics_AnimationFinish(void) {
	bool allAnimationFinished = false;
	if (BoomMutex_Lock()) {
		if (booms == NULL) {
			allAnimationFinished = true;
		}
		BoomMutex_Unlock();
	}
	return allAnimationFinished;
}
void InvaderGraphics_Clear(void) {
	Display_Clear();
}

void InvaderGraphics_StartGame(void) {
	InvaderGraphics_Clear();
	shipHit = false;
	shipImage = Sprite_NewImage(Ship_GetImage());
	if (shipImage != NULL) {
		McuGDisplaySSD1306_PixelDim x = McuGDisplaySSD1306_GetWidth() / 2;
		McuGDisplaySSD1306_PixelDim y = McuGDisplaySSD1306_GetHeight()
				- (shipImage->image->height);
		ship = Sprite_New(shipImage, x, y);
		aliens = NULL;
		alienMissiles = NULL;
		missiles = NULL;
		booms = NULL;
		InvaderGraphics_AlienInit();
	}
}

