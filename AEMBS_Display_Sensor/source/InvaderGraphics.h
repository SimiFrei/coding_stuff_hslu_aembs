/*
 * InvaderGraphics.h
 *
 *  Created on: 10.05.2022
 *      Author: simon
 */

#ifndef INVADERGRAPHICS_H_
#define INVADERGRAPHICS_H_
#include <stdbool.h>
#include "McuGDisplaySSD1306.h"
typedef enum Ship_MoveDirection_e {
	Ship_MoveDir_Unknown,

	Ship_MoveDir_Left,
	Ship_MoveDir_Right,
	Ship_MoveDir_Up,
	Ship_MoveDir_Down,
} Ship_MoveDirection_e;

/* module initialization */
void InvaderGraphics_Init(void);

/* module de-initialization */
void InvaderGraphics_Deinit(void);

/* shows intro text */
void InvaderGraphics_ShowIntro(void);

/* clears display */
void InvaderGraphics_Clear(void);

/* move ship in a direction */
void InvaderGraphics_MoveShip(Ship_MoveDirection_e direction);

/* shoot a missile */
void InvaderGraphics_ShipShootMissile();

/* reacts to events and redraws UI */
void InvaderGraphics_UpdateView(void);

void InvaderGraphics_AlienInit(void);

void MissilesMutex_Unlock(void);
void AliensMutex_Unlock(void);
bool InvaderGraphics_CheckNoAlienLeft(void);
void InvaderGraphics_ShowEnd(bool lost);
void BoomMutex_Unlock(void);
void InvaderGraphics_DrawExplosion(McuGDisplaySSD1306_PixelDim x, McuGDisplaySSD1306_PixelDim y);
void AliensMissileMutex_Unlock(void);
uint8_t GetAliensAlive();
void GetAlienPosition(uint8_t *pos, uint8_t index);
void InvaderGraphics_StartGame(void);
bool GetShipHit(void);
bool InvaderGraphics_AnimationFinish(void);
#endif /* INVADERGRAPHICS_H_ */
