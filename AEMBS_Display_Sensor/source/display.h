/*
 * display.h
 *
 *  Created on: 02.05.2022
 *      Author: simon
 */

#ifndef DISPLAY_H_
#define DISPLAY_H_
#include <stdint.h>
/* module initialization */
void Display_Init(void);

/* module de-initialization */
void Display_Deinit(void);


void Display_UpdateView(void);
void Display_Clear(void);
void Display_DrawRectangle(void);
void Display_DrawCircle(void);
void Display_ShowSensorTemperature(float value);
void Display_ShowGameIntro(void);
void Display_WriteName(uint8_t *surname, uint8_t *name);

#endif /* DISPLAY_H_ */
