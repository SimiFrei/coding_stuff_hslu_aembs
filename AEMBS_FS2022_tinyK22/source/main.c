/*
 * Copyright (c) 2022 Erich Styger
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "platform.h"
#include <stdio.h>
#include "board.h"
#include "peripherals.h"
#include "pin_mux.h"
#include "clock_config.h"
#if PL_CONFIG_IS_TINY
  #include "MK22F51212.h"
#elif PL_CONFIG_IS_LPC
  #include "LPC845.h"
#endif
#include "fsl_debug_console.h"
/* TODO: insert other include files here. */
#include "blinky.h"
#include "cycles_test.h"

int main(void) {
  /* Init board hardware. */
  BOARD_InitBootPins();
  BOARD_InitBootClocks();
  BOARD_InitBootPeripherals();
#ifndef BOARD_INIT_DEBUG_CONSOLE_PERIPHERAL
  /* Init FSL debug console. */
  BOARD_InitDebugConsole();
#endif

  printf("Hello World\n");
  PL_Init();

  Cycles_Test();
  BLINKY_Run();

  while(1) {
    __asm volatile ("nop");
  }
  return 0 ;
}
