################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../McuLib/FreeRTOS/Source/portable/MemMang/heap_1.c \
../McuLib/FreeRTOS/Source/portable/MemMang/heap_2.c \
../McuLib/FreeRTOS/Source/portable/MemMang/heap_3.c \
../McuLib/FreeRTOS/Source/portable/MemMang/heap_4.c \
../McuLib/FreeRTOS/Source/portable/MemMang/heap_5.c \
../McuLib/FreeRTOS/Source/portable/MemMang/heap_useNewlib.c 

OBJS += \
./McuLib/FreeRTOS/Source/portable/MemMang/heap_1.o \
./McuLib/FreeRTOS/Source/portable/MemMang/heap_2.o \
./McuLib/FreeRTOS/Source/portable/MemMang/heap_3.o \
./McuLib/FreeRTOS/Source/portable/MemMang/heap_4.o \
./McuLib/FreeRTOS/Source/portable/MemMang/heap_5.o \
./McuLib/FreeRTOS/Source/portable/MemMang/heap_useNewlib.o 

C_DEPS += \
./McuLib/FreeRTOS/Source/portable/MemMang/heap_1.d \
./McuLib/FreeRTOS/Source/portable/MemMang/heap_2.d \
./McuLib/FreeRTOS/Source/portable/MemMang/heap_3.d \
./McuLib/FreeRTOS/Source/portable/MemMang/heap_4.d \
./McuLib/FreeRTOS/Source/portable/MemMang/heap_5.d \
./McuLib/FreeRTOS/Source/portable/MemMang/heap_useNewlib.d 


# Each subdirectory must supply rules for building sources it contributes
McuLib/FreeRTOS/Source/portable/MemMang/%.o: ../McuLib/FreeRTOS/Source/portable/MemMang/%.c McuLib/FreeRTOS/Source/portable/MemMang/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DCPU_MK22FN512VLH12 -DCPU_MK22FN512VLH12_cm4 -DSDK_OS_BAREMETAL -DSERIAL_PORT_TYPE_UART=1 -DSDK_DEBUGCONSOLE=0 -DCR_INTEGER_PRINTF -DPRINTF_FLOAT_ENABLE=0 -D__MCUXPRESSO -D__USE_CMSIS -DDEBUG -I"D:\hslu\AEMBS\Workspace\AEMBS_SW7_ButtonEvent\drivers" -I"D:\hslu\AEMBS\Workspace\AEMBS_SW7_ButtonEvent\component\serial_manager" -I"D:\hslu\AEMBS\Workspace\AEMBS_SW7_ButtonEvent\utilities" -I"D:\hslu\AEMBS\Workspace\AEMBS_SW7_ButtonEvent\component\uart" -I"D:\hslu\AEMBS\Workspace\AEMBS_SW7_ButtonEvent\component\lists" -I"D:\hslu\AEMBS\Workspace\AEMBS_SW7_ButtonEvent\device" -I"D:\hslu\AEMBS\Workspace\AEMBS_SW7_ButtonEvent\CMSIS" -I"D:\hslu\AEMBS\Workspace\AEMBS_SW7_ButtonEvent\drivers" -I"D:\hslu\AEMBS\Workspace\AEMBS_SW7_ButtonEvent\component\serial_manager" -I"D:\hslu\AEMBS\Workspace\AEMBS_SW7_ButtonEvent\utilities" -I"D:\hslu\AEMBS\Workspace\AEMBS_SW7_ButtonEvent\component\uart" -I"D:\hslu\AEMBS\Workspace\AEMBS_SW7_ButtonEvent\component\lists" -I"D:\hslu\AEMBS\Workspace\AEMBS_SW7_ButtonEvent\device" -I"D:\hslu\AEMBS\Workspace\AEMBS_SW7_ButtonEvent\CMSIS" -I"D:\hslu\AEMBS\Workspace\AEMBS_SW7_ButtonEvent\board" -I"D:\hslu\AEMBS\Workspace\AEMBS_SW7_ButtonEvent\source" -I../McuLib -I../McuLib/config -I../McuLib/config/fonts -I../McuLib/fonts -I../McuLib/src -I../McuLib/FreeRTOS/Source/include -I../McuLib/FreeRTOS/Source/portable/GCC/ARM_CM4F -I../McuLib/SEGGER_RTT -I../McuLib/SEGGER_Sysview -I../McuLib/TraceRecorder -I../McuLib/TraceRecorder/config -I../McuLib/TraceRecorder/include -I../McuLib/TraceRecorder/streamports/Jlink_RTT/include -I../McuLib/HD44780 -I../McuLib/FatFS -I../McuLib/FatFS/source -include"D:/hslu/AEMBS/Workspace/AEMBS_SW7_ButtonEvent/source/IncludeMcuLibConfig.h" -O0 -fno-common -g3 -Wall -c -ffunction-sections -fdata-sections -ffreestanding -fno-builtin -fmerge-constants -fmacro-prefix-map="$(<D)/"= -mcpu=cortex-m4 -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -D__REDLIB__ -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


