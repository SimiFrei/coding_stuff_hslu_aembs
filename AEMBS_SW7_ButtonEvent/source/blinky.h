/*
 * blinky.h
 *
 *  Created on: 25 Mar 2022
 *      Author: simon
 */

#ifndef BLINKY_H_
#define BLINKY_H_


void Blinky_Run(void);

void BlinkyInit(void);

void Blinky_DeInit(void);

#endif /* BLINKY_H_ */
