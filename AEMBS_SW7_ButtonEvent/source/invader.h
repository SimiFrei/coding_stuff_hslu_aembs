/*
 * invader.h
 *
 *  Created on: 7 Apr 2022
 *      Author: simon
 */

#ifndef INVADER_H_
#define INVADER_H_

typedef enum Invader_event_e {
	Invader_Event_None,
	Invader_Event_Button_Left_Pressed,
	Invader_Event_Button_Left_Repeat_Pressed,
	Invader_Event_Button_Left_Released
} t_invader_event_e;

void Invader_SendEvent(t_invader_event_e event);
t_invader_event_e Invader_ReceiveEvent(void);

#endif /* INVADER_H_ */
