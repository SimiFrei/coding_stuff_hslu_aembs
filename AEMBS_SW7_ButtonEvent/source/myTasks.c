/*
 * myTasks.c
 *
 *  Created on: 25 Mar 2022
 *      Author: simon
 */

#include "myTasks.h"
#include "pin_mux.h"
#include "McuRTOS.h"
#include "fsl_gpio.h"
#include <stdint.h>
#include <stdio.h>
#include "McuLib.h"
#include "McuWait.h"
#include "SystemView.h"
#include "invader.h"
static TaskHandle_t buttonTaskHandle = NULL;

static void InvaderTask(void *pv) {

t_invader_event_e event;
	for (;;) {
		event = Invader_ReceiveEvent();
		if(event!=Invader_Event_None){
			switch(event){
			case Invader_Event_Button_Left_Pressed:
				GPIO_PortToggle(BOARD_INITPINS_tinyLED1_GPIO, BOARD_INITPINS_tinyLED1_GPIO_PIN_MASK);
//				GPIO_PortSet(BOARD_INITPINS_tinyLED1_GPIO, BOARD_INITPINS_tinyLED1_GPIO_PIN_MASK);
				break;
			case Invader_Event_Button_Left_Repeat_Pressed:
				break;
			case Invader_Event_Button_Left_Released:
//				GPIO_PortClear(BOARD_INITPINS_tinyLED1_GPIO, BOARD_INITPINS_tinyLED1_GPIO_PIN_MASK);
				break;
			default:
				break;
			}
		}else {
			vTaskDelay(pdMS_TO_TICKS(2));
		}
	}
}


void MyTasksInit() {
	BaseType_t res;
	uint32_t param = 10;
	res = xTaskCreate(InvaderTask, "InvaderTask", 400 / sizeof(StackType_t),
			&param, configMAX_PRIORITIES - 1, &buttonTaskHandle);
	if (res != pdPASS) {
		/*error*/
		for (;;) {
		}
	}

	vTaskStartScheduler();
	// does not get here
}
