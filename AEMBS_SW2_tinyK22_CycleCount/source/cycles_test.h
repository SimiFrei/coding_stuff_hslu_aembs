/*
 * Copyright (c) 2022, Erich Styger
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */


#ifndef CYCLES_TEST_H_
#define CYCLES_TEST_H_

void Cycles_Test(void);

#endif /* CYCLES_TEST_H_ */
