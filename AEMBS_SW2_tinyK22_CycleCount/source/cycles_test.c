/*
 * Copyright (c) 2022, Erich Styger
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "cycles.h"
#include "cycles_test.h"
#include <math.h>
#include <stdio.h>

static float f0 = 1.0f, f1 = 2.0f, f2 = 3.0f;
static double d0 = 1.0, d1 = 2.0, d2 = 3.0;
static volatile int i;

void Cycles_Test(void) {
  uint32_t fputype = SCB_GetFPUType();
  switch(fputype) {
    default:
    case 0: printf("no FPU present\n"); break;
    case 1: printf("single precision FPU present\n"); break;
    case 2: printf("single plus double-precision FPU present\n"); break;
  }

  CCOUNTER_START();
  i = 0;
  CCOUNTER_STOP();
  Cycles_LogTime("test");

  /*! \todo add more tests and measure time */

  CCOUNTER_START();
  printf("Hello World\n");
  CCOUNTER_STOP();
  Cycles_LogTime("printf string");

  CCOUNTER_START();
  printf("value is: %d\n", 1234);
  CCOUNTER_STOP();
  Cycles_LogTime("printf value");

  CCOUNTER_START();
  {
    d0 = sqrt(34.5);
  }
  CCOUNTER_STOP();
  Cycles_LogTime("sqrt");

  CCOUNTER_START();
  {
    d0 = sin(34.5);
  }
  CCOUNTER_STOP();
  Cycles_LogTime("sin");

  CCOUNTER_START();
  puts("Hello World\n");
  Cycles_LogTime("puts");

  CCOUNTER_START();
  {
    const char *str = "Hello World\n";
    size_t len = strlen(str);
    while(len>0) {
      putchar(*str++);
      len--;
    }
  }
  CCOUNTER_STOP();
  Cycles_LogTime("putc");

  CCOUNTER_START();
  f0 = f1+f2;
  CCOUNTER_STOP();
  Cycles_LogTime("fadd");

  CCOUNTER_START();
  d0 = d1+d2;
  CCOUNTER_STOP();
  Cycles_LogTime("dadd");
}
