################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../device/system_MK22F51212.c 

OBJS += \
./device/system_MK22F51212.o 

C_DEPS += \
./device/system_MK22F51212.d 


# Each subdirectory must supply rules for building sources it contributes
device/%.o: ../device/%.c device/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DCPU_MK22FN512VLH12 -DCPU_MK22FN512VLH12_cm4 -DSDK_OS_BAREMETAL -DSERIAL_PORT_TYPE_UART=1 -DSDK_DEBUGCONSOLE=0 -DCR_INTEGER_PRINTF -DPRINTF_FLOAT_ENABLE=0 -D__MCUXPRESSO -D__USE_CMSIS -DDEBUG -I"D:\hslu\AEMBS\Workspace\AEMBS_SW1_tinyK22_Blinky\board" -I"D:\hslu\AEMBS\Workspace\AEMBS_SW1_tinyK22_Blinky\source" -I"D:\hslu\AEMBS\Workspace\AEMBS_SW1_tinyK22_Blinky\drivers" -I"D:\hslu\AEMBS\Workspace\AEMBS_SW1_tinyK22_Blinky\component\serial_manager" -I"D:\hslu\AEMBS\Workspace\AEMBS_SW1_tinyK22_Blinky\utilities" -I"D:\hslu\AEMBS\Workspace\AEMBS_SW1_tinyK22_Blinky\component\uart" -I"D:\hslu\AEMBS\Workspace\AEMBS_SW1_tinyK22_Blinky\component\lists" -I"D:\hslu\AEMBS\Workspace\AEMBS_SW1_tinyK22_Blinky\CMSIS" -I"D:\hslu\AEMBS\Workspace\AEMBS_SW1_tinyK22_Blinky\device" -O0 -fno-common -g3 -Wall -c -ffunction-sections -fdata-sections -ffreestanding -fno-builtin -fmerge-constants -fmacro-prefix-map="$(<D)/"= -mcpu=cortex-m4 -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -D__REDLIB__ -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


